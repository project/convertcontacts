Module: ConvertContacts
Author: ReachLocal <https://www.drupal.org/user/3405578>
Tested up to: 8.6.1
Stable tag: 1.0
License: MIT
License URI: https://opensource.org/licenses/MIT

Description
===========
For ConvertContacts customers ConvertContacts provides a simple Drupal module that enables you to capture leads, understand your sources of leads, and provides tools to help you respond to and manage those leads.

The ConvertContacts Drupal module adds the tracking code to the Drupal site. This module adds the ConvertContacts software on all pages of the Drupal website. The software is loaded from a CDN and is under continuous development to provide the best performance and stability across all browser and OS combinations.  As new features and functionality are added to ConvertContacts, those updates will be rolled out without any updates of this module.

About ConvertContacts
ConvertContacts, [ReachLocal](http://www.reachlocal.com)’s lead conversion software, lets you manage leads and turn them into customers. The lead tracking technology in ConvertContacts measures how each of your marketing sources - like search advertising, social media, and SEO - is working. Plus, it records every phone call that comes from your website so you know exactly how they found and listen to and respond to inquiries.

With ConvertContacts, you also get access to an online dashboard and mobile application that provide you visibility into your inbound leads and tools to follow up with these leads - such as by editing the lead status and assigning the leads to other people in your company so you an follow up with them.


Requirements
============

* ConvertContacts Site ID


Installation
============
* Copy the 'convertcontacts' module directory in to your Drupal
sites/all/modules directory as usual.


Usage
=====
In the settings page enter your ConvertContacts Site ID.

All pages will now have the required JavaScript added to the
HTML header can confirm this by viewing the page source from
your browser.

By default the following pages are excluded from loading ConvertContacts:

admin
admin/*
batch
node/*
node/*/*
user/*
user/*/*

Known issues
============

None
