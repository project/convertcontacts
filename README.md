# ConvertContacts Drupal Module
ConvertContacts provides a simple Drupal module, enabling you to capture leads, understand your sources of leads, respond to and manage those leads.  The ConvertContacts Drupal extension adds the tracking software on all the pages of your website.


## Features

* Enables the ConvertContacts tracking functionality on Drupal sites. 


## Installation

1. Download the [latest stable
   release](http://github.com/reachlocal/convert_contacts_drupal_module_8x/releases/latest).
2. In the Drupal dashboard, navigate to the *Extend* page and click *Install new module*, then click *Choose File*.
3. Browse to the 'convert_contacts_drupal_module_8x.zip' file, then select *Open* and click *Install Now*.
4. When the upload and installation completes successfully, click on *Enable newly added modules*.
5. Locate ConvertContacts under the Lead Conversion section, Click the *Enable* checkbox, then Install

### Entering your tracking code ID

1. In the Drupal dashboard, navigate to the *Configuration* menu.
3. Click the *ConvertContacts* link.
2. Enter your tracking code ID into the ID field, and click the *Save Configuration* button.

